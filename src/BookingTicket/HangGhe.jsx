import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { datGheAction } from "../Store/action/BookingTicketAction";
class HangGhe extends Component {
  handleDatGhe = (ghe) => {
    this.props.dispatch(
      // type: "DAT_GHE",
      // payload: { ghe },
      datGheAction(ghe)
    );
  };
  renderGhe = () => {
    return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
      let cssGheDaDat = "";
      let disabled = false;
      // Trang thai ghe đã đc đặt
      if (ghe.daDat) {
        cssGheDaDat = "gheDuocChon";
        disabled = true;
      }

      // Xet trang thai ghe đang đặt
      let cssGheDangDat = "";
      let indexGheDangDat = this.props.danhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.soGhe === ghe.soGhe
      );
      if (indexGheDangDat !== -1) {
        cssGheDangDat = "gheDangChon";
      }
      return (
        <button
          onClick={() => {
            this.handleDatGhe(ghe);
          }}
          disabled={disabled}
          className={`ghe ${cssGheDaDat} ${cssGheDangDat}`}
          key={index}
        >
          {ghe.soGhe}
        </button>
      );
    });
  };

  renderSoHang = () => {
    return this.props.hangGhe.danhSachGhe.map((hang, index) => {
      return (
        <button key={index} className="rowNumber">
          {hang.soGhe}
        </button>
      );
    });
  };

  renderHangGhe = () => {
    if (this.props.soHangGhe == 0) {
      return (
        <div className="ml-4">
          {this.props.hangGhe.hang}
          {this.renderSoHang()}
        </div>
      );
    } else {
      return (
        <Fragment>
          {this.props.hangGhe.hang}
          {this.renderGhe()}
        </Fragment>
      );
    }
  };

  render() {
    return (
      <div className="text-light text-left ml-5 mt-3" style={{ fontSize: 30 }}>
        {this.renderHangGhe()}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.bookingTicket.danhSachGheDangDat,
  };
};

export default connect(mapStateToProps)(HangGhe);
