import React, { Component, Fragment } from "react";
import "./BaiTapBookingTicket.css";
import ThongTinDatGhe from "./ThongTinDatGhe";
import danhSachGheData from "./Data/danhSachGhe.json";
import HangGhe from "./HangGhe";
class Index extends Component {
  renderHangGhe = () => {
    return danhSachGheData.map((hangGhe, index) => {
      return (
        <Fragment key={index}>
          <HangGhe hangGhe={hangGhe} soHangGhe={index} />
        </Fragment>
      );
    });
  };

  render() {
    return (
      <div
        style={{
          position: "relative",
          width: "100%",
          height: "100%",
          background: "url('./img/bookingTicket/bgmovie.jpg')",
          backgroundSize: "100%",
        }}
        className="bookingMovie"
      >
        <div
          style={{
            backgroundColor: "rgba(0,0,0,0.6)",
            position: "relative",
            width: "100%",
            height: "100%",
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-8 text-center">
                <h1 className="text-warning display-4">BOOKING MOVIES</h1>
                <div style={{ fontSize: "25px" }} className="mt-5 text-light">
                  SCREEN
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  }}
                  className="mt-1"
                >
                  <div className="screen"></div>
                  {this.renderHangGhe()}
                </div>
              </div>
              <div className="col-4">
                <h1
                  style={{ fontSize: "35px" }}
                  className="text-light display-4 mt-2"
                >
                  BOOKED LIST
                </h1>
                <ThongTinDatGhe />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
