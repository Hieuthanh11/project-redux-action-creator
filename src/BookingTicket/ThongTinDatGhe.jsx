import React, { Component } from "react";
import { connect } from "react-redux";
import { huyGheAction } from "../Store/action/BookingTicketAction";
import { HUY_GHE } from "../Store/types/BookingTicketType";
class ThongTinDatGhe extends Component {
  render() {
    return (
      <div>
        <div className="mt-5">
          <button className="gheDuocChon"></button>
          <span className="text-light" style={{ fontSize: "30px" }}>
            BOOKED
          </span>
          <br />
          <button className="gheDangChon"></button>
          <span className="text-light" style={{ fontSize: "30px" }}>
            CHOOSEN
          </span>
          <br />
          <button className="ghe ml-0"></button>
          <span className="text-light" style={{ fontSize: "30px" }}>
            EMPTY
          </span>
        </div>
        <div className="mt-5">
          <table className="table" border="2">
            <thead>
              <tr className="text-light" style={{ fontSize: 20 }}>
                <th>Number</th>
                <th>Price</th>
                <th></th>
              </tr>
            </thead>
            <tbody className="text-warning">
              {this.props.danhSachGheDangDat.map((gheDangDat, index) => {
                return (
                  <tr key={index}>
                    <td>{gheDangDat.soGhe}</td>
                    <td>{gheDangDat.gia.toLocaleString()}</td>
                    <td>
                      <button
                        onClick={() =>
                          this.props.dispatch(huyGheAction(gheDangDat.soGhe))
                        }
                      >
                        Remove
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tfoot>
              <tr className="text-center text-primary" style={{ fontSize: 20 }}>
                <td colSpan="2">Total</td>
                <td>
                  {this.props.danhSachGheDangDat
                    .reduce((total, gheDangDat) => {
                      return (total += gheDangDat.gia);
                    }, 0)
                    .toLocaleString()}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.bookingTicket.danhSachGheDangDat,
  };
};
export default connect(mapStateToProps)(ThongTinDatGhe);
