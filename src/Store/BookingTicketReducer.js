import { DAT_GHE, HUY_GHE } from "./types/BookingTicketType";

const initialState = {
  danhSachGheDangDat: [],
};

const reducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case DAT_GHE: {
      let cloneDanhSachGheDangDat = [...state.danhSachGheDangDat];
      let index = cloneDanhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.soGhe === action.ghe.soGhe
      );
      if (index !== -1) {
        cloneDanhSachGheDangDat.splice(index, 1);
      } else {
        cloneDanhSachGheDangDat.push(action.ghe);
      }
      state.danhSachGheDangDat = cloneDanhSachGheDangDat;
      return { ...state };
    }

    case HUY_GHE: {
      let cloneDanhSachGheDangDat = [...state.danhSachGheDangDat];
      let index = cloneDanhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.soGhe === action.soGhe
      );
      if (index !== -1) {
        cloneDanhSachGheDangDat.splice(index, 1);
      }
      state.danhSachGheDangDat = cloneDanhSachGheDangDat;
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default reducer;
